# chrootbuilder

## Usage

Usage:
```
chrootbuilder [--help] [--container] [--preserve] [--extract] [--docker-timeout] name location
 ```
Build chroot images (tarball) from docker images/containers

Name can refer to a docker image's name or id. If you wish to specify a container's name or id,
add the `--container` option. Location represents where to write the tarred filesystem to on disk.
Note that this location cannot exist. Likewise if `--extract` is specified, the location must be a directory, 
or it cannot exist.

The exported chroot image might look different than the docker container/image it was built from.
This is for a few reasons, the first is that chrootbuilder will modify the chroot image by default,
so that it contains a /tmp directory and an empty /dev directory. This is so that the exported chroot
image maintains compatibility with buildbox-run-userchroot. This behaviour can be disabled by passing
in the `-p` (`--preserve`) flag.

The second reason is that volumes/mounts will not be exported properly. They will
either be excluded or appear empty. Similiarly, device files will also be missing or appear empty.
Unfortunately, this behaviour cannot be disabled. It is important to note that chrootbuilder
will not modify the underlying docker image/container in any way; consequently you can build
your chroots safely without fear of side effects.

It should also be noted that running chrootbuilder with the `-c` option and not the `-p`
option, requires the container to have tar installed.

The docker api client timeout can be changed by specifying the `-t` (`--docker-timeout`) as an optional integer

## Example Usages:

Export filesystem in docker image `docker_name` to `chroot.tar` in current working directory:
```
chrootbuilder docker_name chroot.tar
```

Export and unpack filesystem in docker image `docker_name` to chroot directory in current working directory:
```
chrootbuilder -e docker_name chroot
```

Export docker container `my_container` to `/location/to/extract.tar`:
```
chrootbuilder -c my_container /location/to/extract.tar
```

Do not modify the chroot image at all when exporting (modified by default to be compatabile with buildbox-run-userchroot):
```
chrootbuilder -p docker_name chroot.tar
```

For further examples, refer to [this Dockerfile](https://gitlab.com/BuildGrid/buildbox/buildbox-run-userchroot/-/blob/master/compose-data/Dockerfile.recc) to see how 
it is used to upload an Alpine image to a CAS.

## Installing:
```
pip install .
```
